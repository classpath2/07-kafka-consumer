package io.classpath.kafka.examples.config;

public class AppConfig {
    public final static String groupID = "simpleconsumer";
    public final static String bootstrapServers = "157.245.108.69:9092";
    public final static String topicName = "message-consumer";
}