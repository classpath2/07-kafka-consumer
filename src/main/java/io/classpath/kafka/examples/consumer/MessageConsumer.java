package io.classpath.kafka.examples.consumer;

import io.classpath.kafka.examples.config.AppConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.function.Supplier;

public class MessageConsumer {
    private static final Logger logger = LogManager.getLogger();
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(ConsumerConfig.GROUP_ID_CONFIG, AppConfig.groupID);
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.bootstrapServers);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        InputStream input = null;
        KafkaConsumer<Integer, String> consumer = null;


        try {
            consumer = new KafkaConsumer<>(props);
            logger.info("Created a Kafka Consumer::");
            consumer.subscribe(Arrays.asList(AppConfig.topicName));

            while (true) {
                Thread.sleep(2000);
                ConsumerRecords<Integer, String> records = consumer.poll(100);
                logger.info("Records : "+ records.count());
                for (ConsumerRecord<Integer, String> record : records) {
                    logger.info("Value is= " + record.value());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            consumer.close();
        }
    }
}